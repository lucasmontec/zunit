# ZUnit
## A simpler slimer testing framework for Unity 3D

This is a testing framework designed to be small and easy to use.
You can extend it to connect with your CI/CD pipeline.
It doesn't require a test assembly to be built, you can just use it.

I'm still working on it. Currently this is a WIP project.
