start /wait F:\Unity\2018.4.0f1\Editor\Unity.exe -projectPath . -batchmode -nographics -logfile log.txt -executeMethod ZUnit.Editor.IntegrationRunner.RunPlayModeTests

if errorlevel 1 (
   echo Failure Reason Given is %errorlevel%
   exit /b %errorlevel%
)