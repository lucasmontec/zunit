using System.Collections.Generic;
using System.Reflection;
using ZUnit.Editor;
using ZUnit.Editor.Reporting;
using ZUnit.Filtering;

namespace ZUnit
{
    public interface ITestRunner
    {
        Dictionary<MethodInfo, TestResultInfo> RunTestEntry(TestEntry testEntry, TestFilter filter = null);

        void RunEntriesTests(IEnumerable<TestEntry> testEntries, TestFilter filter = null);
        
        void RunEntriesTestsReportAndClose(TestSuite testSuite, IEnumerable<TestEntry> testEntries,
            ReportType reportType, TestFilter filter = null);
    }
}