using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace ZUnit.TestRunning
{
    public static class TestRunningUtils
    {
        public static object CreateTestTypeInstance(Type t)
        {
            //Play mode instances require game objects
            if (t.IsSubclassOf(typeof(MonoBehaviour)) && EditorApplication.isPlaying)
            {
                GameObject testObject = new GameObject($"TestObject_{t.Name}");
                return testObject.AddComponent(t);
            }

            try
            {
                return Activator.CreateInstance(t);
            }
            catch (ArgumentNullException)
            {
                Debug.LogError("Failed to create an instance for the test type. " +
                               "The type is null.");
            }
            catch (ArgumentException)
            {
                Debug.LogError("Failed to create an instance for the test type. " +
                               "The type is not a runtime type.");
            }
            catch (NotSupportedException)
            {
                Debug.LogError("Failed to create an instance for the test type. " +
                               "The type cannot be created by reflection.");
            }

            return null;
        }
        
                /// <summary>
        /// Runs a test method by calling the test run delegate.
        /// Runs the correct method-wrapping setup and tear down methods.
        /// </summary>
        /// <param name="testEntry">The test entry to access the setup methods.</param>
        /// <param name="testInstance">The test instance to run the method in.</param>
        /// <param name="methodInfo">The method info to know which method to run.</param>
        /// <returns>The test result.</returns>
                public static TestResultInfo RunSingleMethod(
            TestEntry testEntry,
            object testInstance,
            MethodBase methodInfo)
        {
            //Run the prepare method if any
            if (testEntry.BeforeMethod != null)
            {
                testEntry.BeforeMethod.Invoke(testInstance, null);
            }

            //Runs the test method
            TestResultInfo result = RunTestMethod(methodInfo, testInstance, testEntry.TestType.Name);

            //Run the tear down method if any
            if (testEntry.AfterMethod != null)
            {
                testEntry.AfterMethod.Invoke(testInstance, null);
            }

            return result;
        }

        /// <summary>
        /// Runs a simple test method that doesn't require play mode.
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="testInstance"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static TestResultInfo RunTestMethod(MethodBase methodInfo, object testInstance, string typeName)
        {
            TestResultInfo result = new TestResultInfo {methodName = methodInfo.Name};
            
            try
            {
                methodInfo.Invoke(testInstance, null);
                result.result = TestResult.Passed;
            }
            catch (TargetInvocationException e)
            {
                result = FailedResultInfoFromException(methodInfo, e);
            }

            return result;
        }

        public static TestResultInfo FailedResultInfoFromException(MemberInfo methodInfo, Exception e)
        {
            TestResultInfo result = new TestResultInfo(){methodName = methodInfo.Name};
            //First we need to skip the invocation exception to get our cause
            Exception inner = e.InnerException ?? e;

            // Get stack trace for the exception with source file information
            var st = new StackTrace(inner, true);
            // Get the top+1 stack frame (since the top is the assert library method that we need to ignore)
            StackFrame frameForAssert = st.GetFrame(1);
            // Get the line number from the stack frame
            var lineForAssert = frameForAssert.GetFileLineNumber();

            StackFrame frameForNormalException = st.GetFrame(0);

            var line = lineForAssert;
            var filePath = FindPathForType(methodInfo.DeclaringType);
            
            //If the exception was not thrown by ZAssert we skip, else we get the original exception
            if (!inner.StackTrace.Contains("ZAssert"))
            {
                line = frameForNormalException.GetFileLineNumber();
                filePath = CutPathToProjectFolder(frameForNormalException.GetFileName());
            }

            /*Debug.LogError($"Test failed with an exception: {methodInfo.Name} {e.Message}" +
                               $"in type {methodInfo.DeclaringType.Name} at line {line}.\n{inner.StackTrace}");*/

            result.result = TestResult.Failed;
            result.failLineNumber = line;
            result.filePath = filePath;
            result.typeName = methodInfo.DeclaringType?.Name;
            result.exception = inner;
            return result;
        }

        private static string FindPathForType(MemberInfo type)
        {
            string[] fileNames = Directory.GetFiles(
                Application.dataPath, $"{type.Name}.cs", SearchOption.AllDirectories);

            string path = fileNames.Length > 0 ? Path.GetFullPath(fileNames[0]) : "";

            path = CutPathToProjectFolder(path);

            return path;
        }

        private static string CutPathToProjectFolder(string path)
        {
            if (path == null) return "";
            
            //Cut to project folder
            int indexOfAssets = path.IndexOf("Assets", StringComparison.Ordinal);
            return indexOfAssets < 0 ? "" : path.Substring(indexOfAssets);
        }
    }
}