﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using ZUnit.Editor.Reporting;
using ZUnit.Filtering;

namespace ZUnit.Editor.TestRunning
{
   public class PlayModeTestData : SerializedScriptableObject
   {
      public List<TestEntry> playModeTestEntries = new List<TestEntry>();

      /// <summary>
      /// The filter to run the play mode tests.
      /// </summary>
      public TestFilter filter;
      
      /// <summary>
      /// Set to true so the editor will save a test report and close.
      /// </summary>
      public bool generateReportAndClose;

      /// <summary>
      /// If running in a pipeline, this is the report that the editor will save.
      /// </summary>
      public ReportType reportType = ReportType.Markdown;
   }
}
