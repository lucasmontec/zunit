using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZUnit.Attributes;
using ZUnit.Editor;
using ZUnit.Editor.Reporting;
using ZUnit.Editor.TestRunning;
using ZUnit.Filtering;
using ZUnit.Utils;
using Debug = UnityEngine.Debug;

namespace ZUnit.TestRunning
{
    public class PlayModeTestRunner : ITestRunner
    {
        private const string PLAY_MODE_ASSET_PATH = "Assets/PlayModeTestData.asset";

        /// <summary>
        /// The number of currently running coroutine tests.
        /// </summary>
        private static int runningCoroutineTests = 0;

        /// <summary>
        /// The game object that runs coroutine tests when they are not inside mono behaviours.
        /// </summary>
        private static MonoBehaviour coroutineRunner;

        /// <summary>
        /// This is set to true if the call is from a pipeline/commandLine.
        /// </summary>
        private static bool runningFromCommandLine;

        /// <summary>
        /// If the tests are running from the command line, this is the report the system generates.
        /// </summary>
        private static ReportType reportType;
        
        public void RunEntriesTests(IEnumerable<TestEntry> testEntries, TestFilter filter = null)
        {
            bool hasPlayModeTests = false;
            
            List<TestEntry> playModeTestEntries = new List<TestEntry>();
            
            //Check all entries
            foreach (TestEntry testEntry in testEntries)
            {
                if (!testEntry.PlayModeTestMethods.Any()) continue;
                
                //Filter tests, do not include entries that don't match the filter
                if(filter != null && !testEntry.PlayModeTestMethods.Any(filter.Check)) return;
                
                //Store play mode tests to run in play mode
                playModeTestEntries.Add(testEntry);
                hasPlayModeTests = true;
            }
            
            //If we have play mode tests, we need to start play mode
            if (!hasPlayModeTests) return;

            //Save the current scene if necessary
            if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                Debug.LogWarning("User canceled play mode tests.");
                return;
            }

            //Store the play mode test data so it survives the play mode
            PlayModeTestData testData = ScriptableObject.CreateInstance<PlayModeTestData>();
            testData.playModeTestEntries.AddRange(playModeTestEntries);
            
            //Add the filter
            testData.filter = filter;
            
            //Check command line
            if (runningFromCommandLine)
            {
                //If we are running from the command line we need to
                //prepare to generate the report and close the editor
                testData.generateReportAndClose = runningFromCommandLine;
                testData.reportType = reportType;
            }
            
            //Save the play mode data
            AssetDatabase.CreateAsset(testData, PLAY_MODE_ASSET_PATH);
            AssetDatabase.SaveAssets();
            
            //Start the play mode
            EditorApplication.isPlaying = true;
        }

        /// <summary>
        /// Method to run tests from the command line.
        /// </summary>
        /// <param name="testSuite">The suite calling for testing.</param>
        /// <param name="testEntries">The entries to test,</param>
        /// <param name="type">The type of test report to generate.</param>
        /// <param name="filter">The filter to filter the running methods</param>
        public void RunEntriesTestsReportAndClose(TestSuite testSuite, IEnumerable<TestEntry> testEntries, ReportType type, TestFilter filter = null)
        {
            runningFromCommandLine = true;
            reportType = type;
            
            RunEntriesTests(testEntries, filter);
        }

        [RuntimeInitializeOnLoadMethod]
        private static void EnteredPlayModeState()
        {
            Dictionary<MethodInfo, TestResultInfo> testResults = new Dictionary<MethodInfo, TestResultInfo>();
            
            if (TestResultsSerializer.HasTestResults())
            {
                testResults.AddRange(TestResultsSerializer.LoadResults());
            }

            //Check if we have tests to run
            PlayModeTestData testData = AssetDatabase.LoadAssetAtPath<PlayModeTestData>(PLAY_MODE_ASSET_PATH);
            
            //Only run if we have something to run
            if(!testData) return;
            
            //Create the tests scene
            Scene testsScene = SceneManager.CreateScene("TestScene");
            SceneManager.SetActiveScene(testsScene);

            //Read the filter
            TestFilter filter = testData.filter;
            
            //Run tests
            foreach (TestEntry testEntry in testData.playModeTestEntries)
            {
                //Create the instance to run the methods
                object testInstance = TestRunningUtils.CreateTestTypeInstance(testEntry.TestType);

                //Run the class before method if any
                if (testEntry.BeforeClassMethod != null)
                {
                    testEntry.BeforeClassMethod.Invoke(testInstance, null);
                }

                //Run each method in that instance
                foreach (MethodInfo methodInfo in testEntry.PlayModeTestMethods.Where(mi => filter == null || filter.Check(mi)))
                {
                    //Prepare method with default result
                    testResults[methodInfo] = TestResultInfo.Inconclusive();
                    
                    //Check if the method to run is a coroutine
                    if (methodInfo.ReturnType == typeof(IEnumerator))
                    {
                        //Run coroutine tests
                        runningCoroutineTests++;

                        //Create a game object to run the coroutine tests
                        if (!testInstance.GetType().IsInstanceOfType(typeof(MonoBehaviour)))
                        {
                            GameObject runnerParent = new GameObject("CoroutineRunner");
                            coroutineRunner = runnerParent.AddComponent<CoroutineRunner>();
                        }
                        else
                        {
                            coroutineRunner = (MonoBehaviour) testInstance;
                        }
                        
                        RunCoroutineMethod(testEntry, testInstance, coroutineRunner, methodInfo,
                            //When finished, handle ending
                            info =>
                            {
                                //Update the result and save
                                info.playModeTest = true;
                                testResults[methodInfo] = info;

                                //Save the results
                                TestResultsSerializer.SaveResults(testResults, testData.generateReportAndClose, testData.reportType);
                                
                                //Decrease running routines and check if we ended tests
                                runningCoroutineTests--;
                                if (runningCoroutineTests == 0)
                                {
                                    EditorApplication.isPlaying = false;
                                }
                            });
                    }
                    else
                    {
                        //Run normal method in play mode
                        TestResultInfo runSingleMethod = TestRunningUtils.RunSingleMethod(testEntry, testInstance, methodInfo);
                        //Tag as a play mode result
                        runSingleMethod.playModeTest = true;
                        //Store
                        testResults[methodInfo] = runSingleMethod;
                    }
                }

                //Run the class after (tear down) method if any
                if (testEntry.AfterClassMethod != null)
                {
                    testEntry.AfterClassMethod.Invoke(testInstance, null);
                }
            }

            //Save the results
            TestResultsSerializer.SaveResults(testResults, testData.generateReportAndClose, testData.reportType);

            //Testing done, delete them
            AssetDatabase.DeleteAsset(PLAY_MODE_ASSET_PATH);
            
            //Disable play mode
            if(runningCoroutineTests == 0) EditorApplication.isPlaying = false;
        }

        /// <summary>
        /// Returns a partial result (Failed or Inconclusive).
        /// The result is returned async when the coroutine ends.
        /// </summary>
        /// <param name="testEntry"></param>
        /// <param name="testInstance"></param>
        /// <param name="testCoroutineRunner"></param>
        /// <param name="methodInfo"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        private static TestResultInfo RunCoroutineMethod(
            TestEntry testEntry,
            object testInstance,
            MonoBehaviour testCoroutineRunner,
            MethodBase methodInfo,
            Action<TestResultInfo> onResult)
        {
            //Run the prepare method if any
            if (testEntry.BeforeMethod != null)
            {
                testEntry.BeforeMethod.Invoke(testInstance, null);
            }

            //Runs the test method
            TestResultInfo result = RunCoroutineTestMethod(
                methodInfo,
                testCoroutineRunner,
                testInstance, 
                testEntry.TestType.Name,
                onResult);

            //Run the tear down method if any
            if (testEntry.AfterMethod != null)
            {
                testEntry.AfterMethod.Invoke(testInstance, null);
            }

            return result;
        }
        
        /// <summary>
        /// Returns a partial result (Failed or Inconclusive).
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="runner"></param>
        /// <param name="testInstance"></param>
        /// <param name="typeName"></param>
        /// <param name="onResult"></param>
        /// <returns></returns>
        public static TestResultInfo RunCoroutineTestMethod(
            MethodBase methodInfo,
            MonoBehaviour runner,
            object testInstance,
            string typeName,
            Action<TestResultInfo> onResult)
        {
            //Create the result with an intermediate result.
            TestResultInfo result = new TestResultInfo()
            {
                result = TestResult.Inconclusive,
                methodName = methodInfo.Name
            };
            
            try
            {
                //Start the coroutine
                runner.StartCoroutine(
                    //Get a wrapping coroutine that handles exceptions from the method
                CoroutineUtils.RunThrowingIterator((IEnumerator)methodInfo.Invoke(testInstance, null), 
                    //When the coroutine is done, report
                    exception =>
                    {
                        TestResultInfo callbackResult;
                        if (exception != null)
                        {
                            callbackResult = TestRunningUtils.FailedResultInfoFromException(methodInfo, exception);
                        }
                        else
                        {
                            callbackResult = new TestResultInfo
                            {
                                result = TestResult.Passed
                            };
                        }
                        
                        onResult?.Invoke(callbackResult);
                    })
                );
            }
            catch (TargetInvocationException e)
            {
                result = TestRunningUtils.FailedResultInfoFromException(methodInfo, e);
            }

            return result;
        }
        
        public Dictionary<MethodInfo, TestResultInfo> RunTestEntry(TestEntry testEntry, TestFilter filter = null)
        {
            Debug.LogWarning("Play mode test runner can only run a list of tests for now.");
            return new Dictionary<MethodInfo, TestResultInfo>();
        }
    }
}
