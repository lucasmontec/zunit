using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using ZUnit.Editor;
using ZUnit.Editor.Reporting;
using ZUnit.Filtering;
using ZUnit.Utils;
using Debug = UnityEngine.Debug;

namespace ZUnit.TestRunning
{
    [Serializable]
    public class TestRunner : ITestRunner
    {
        public Dictionary<MethodInfo, TestResultInfo> RunTestEntry(TestEntry testEntry, TestFilter filter = null)
        {
            Dictionary<MethodInfo, TestResultInfo> testResults = new Dictionary<MethodInfo, TestResultInfo>();
            
            if (TestResultsSerializer.HasTestResults())
            {
                testResults.AddRange(TestResultsSerializer.LoadResults());
            }

            //Don't run runtime tests in edit mode
            if (testEntry.TestType.IsSubclassOf(typeof(MonoBehaviour)))
            {
                if (testEntry.TestMethods.Count > 0)
                {
                    Debug.LogWarning($"Normal tests for class {testEntry.TestType.Name} won't run. " +
                                     $"Normal tests can only be run in non behaviour classes!");
                }
                return testResults;
            }
            
            //Create an instance of the test class to run all the tests
            object testInstance = TestRunningUtils.CreateTestTypeInstance(testEntry.TestType);

            //Check if the instance is valid
            if (testInstance == null)
            {
                //Debug.LogError("Cannot run tests for this type. Skipping.");
                return testResults;
            }

            //Run the class before method if any
            if (testEntry.BeforeClassMethod != null)
            {
                testEntry.BeforeClassMethod.Invoke(testInstance, null);
            }

            //Run all normal tests
            foreach (MethodInfo methodInfo in testEntry.TestMethods.Where(mi => filter == null || filter.Check(mi)))
            {
                testResults[methodInfo] = TestRunningUtils.RunSingleMethod(testEntry, testInstance, methodInfo);
            }

            //Run the class after (tear down) method if any
            if (testEntry.AfterClassMethod != null)
            {
                testEntry.AfterClassMethod.Invoke(testInstance, null);
            }
            
            //Save the results
            TestResultsSerializer.SaveResults(testResults);

            return testResults;
        }

        /// <summary>
        /// Runs several test entries.
        /// Results are saved during test running.
        /// </summary>
        /// <param name="testEntries"></param>
        /// <param name="filter"></param>
        public void RunEntriesTests(IEnumerable<TestEntry> testEntries, TestFilter filter = null)
        {
            foreach (TestEntry testEntry in testEntries)
            {
                RunTestEntry(testEntry);
            }
        }

        public void RunEntriesTestsReportAndClose(TestSuite testSuite, IEnumerable<TestEntry> testEntries, ReportType type, TestFilter filter = null)
        {
            RunEntriesTests(testEntries);

            switch (type)
            {
                case ReportType.Markdown:
                    TestReporter.SaveMarkdownReport(testSuite);
                    break;
                case ReportType.Json:
                    TestReporter.SaveJsonReport(testSuite);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}