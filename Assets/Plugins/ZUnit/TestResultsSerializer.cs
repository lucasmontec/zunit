using System;
using System.Collections.Generic;
using System.Reflection;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
using ZUnit.Editor.Reporting;

namespace ZUnit.Editor
{
    public static class TestResultsSerializer
    {
        private const string TEST_RESULTS_KEY = "test_result_";

        public static bool HasTestResults(string key="default")
        {
            return EditorPrefs.HasKey($"{TEST_RESULTS_KEY}{key}");
        }
        
        public static void RemoveSavedResults(string key="default")
        {
            EditorPrefs.DeleteKey($"{TEST_RESULTS_KEY}{key}");
        }
        
        public static void SaveResults(Dictionary<MethodInfo, TestResultInfo> results, bool reportAndClose=false, 
            ReportType reportType=ReportType.Markdown, string key="default")
        {
            //Serialize the results dictionary
            byte[] serializedResult = SerializationUtility.SerializeValue(results, DataFormat.Binary);
            //Convert to base64 to save them
            string base64Result = Convert.ToBase64String(serializedResult);
            //Store them in the editor prefs
            EditorPrefs.SetString($"{TEST_RESULTS_KEY}{key}", base64Result);
            
            //Save reportAndClose and save report type
            EditorPrefs.SetBool($"{TEST_RESULTS_KEY}_reportAndClose", reportAndClose);
            EditorPrefs.SetString($"{TEST_RESULTS_KEY}_reportType", reportType.ToString());
        }

        public static Dictionary<MethodInfo, TestResultInfo> LoadResults(out bool reportAndClose, out ReportType reportType, string key="default")
        {
            //Read report and close and report type
            reportAndClose = EditorPrefs.GetBool($"{TEST_RESULTS_KEY}_reportAndClose", false);
            string reportTypeString = EditorPrefs.GetString($"{TEST_RESULTS_KEY}_reportType", ReportType.Markdown.ToString());
            Enum.TryParse(reportTypeString, true, out reportType);
            
            //If the deserialization worked, return the results
            return LoadResults(key);
        }
        
        public static Dictionary<MethodInfo, TestResultInfo> LoadResults(string key="default")
        {
            //Get the saved base 64 results
            string base64Results = EditorPrefs.GetString($"{TEST_RESULTS_KEY}{key}", null);

            if (base64Results == null)
            {
                Debug.LogWarning($"Failed to find test results for key: {key}");
                return new Dictionary<MethodInfo, TestResultInfo>();;
            }
            
            //Convert to byte array
            byte[] serializedResult = Convert.FromBase64String(base64Results);
            
            //Use odin to deserialize the results
            Dictionary<MethodInfo, TestResultInfo> savedResults = SerializationUtility
                .DeserializeValue<Dictionary<MethodInfo, TestResultInfo>>
                    (serializedResult,DataFormat.Binary);

            //If the deserialization worked, return the results
            return savedResults ?? new Dictionary<MethodInfo, TestResultInfo>();
        }
        
    }
}