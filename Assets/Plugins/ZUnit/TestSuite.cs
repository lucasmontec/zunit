using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using UnityEditor;
using ZUnit.Attributes;
using ZUnit.Editor.Reporting;
using ZUnit.Filtering;
using ZUnit.Utils;
using Debug = UnityEngine.Debug;

namespace ZUnit.Editor
{
    
    [Serializable]
    public class TestSuite
    {
        private const string PM_TESTING_KEY = "pm_testing";

        public Dictionary<MethodInfo, TestResultInfo> TestResults { get; private set; } 
            = new Dictionary<MethodInfo, TestResultInfo>();

        private readonly ITestRunner testRunner;
        private readonly ITestRunner playModeTestRunner;
        
        private readonly Dictionary<Type, TestEntry> testEntries = new Dictionary<Type, TestEntry>();

        /// <summary>
        /// Returns true if the suite did play mode tests.
        /// </summary>
        public static bool DidPlayModeTests => EditorPrefs.GetBool(PM_TESTING_KEY, false);

        /// <summary>
        /// Resets the play mode tests flag so we don't reload the results every
        /// play mode state switch.
        /// </summary>
        public static void ResetPlayModeTestsFlag()
        {
            //Save that we already read play mode tests
            EditorPrefs.SetBool(PM_TESTING_KEY, false);
        }
        
        public TestSuite(ITestRunner testRunner, ITestRunner playModeTestRunner)
        {
            this.testRunner = testRunner;
            this.playModeTestRunner = playModeTestRunner;
        }

        public void LoadTestResults(out bool reportAndClose, out ReportType reportType)
        {
            if (TestResultsSerializer.HasTestResults())
            {
                TestResults.Clear();
                TestResults.AddRange(TestResultsSerializer.LoadResults(out reportAndClose, out reportType));
            }
            else
            {
                reportAndClose = false;
                reportType = ReportType.Markdown;
            }
        }
        
        public void RunPlayModeTestsReportAndClose(ReportType reportType)
        {
            //Run all play mode tests
            playModeTestRunner.RunEntriesTestsReportAndClose(this, testEntries.Values, reportType);
        }
        
        /// <summary>
        /// Loads test results from the editor prefs.
        /// Test results are saved during assembly reloads.
        /// </summary>
        public void LoadTestResults()
        {
            if (!TestResultsSerializer.HasTestResults()) return;
            TestResults.Clear();
            TestResults.AddRange(TestResultsSerializer.LoadResults());
        }

        /// <summary>
        /// Runs all normal loaded tests.
        /// Play mode tests are ignored.
        /// </summary>
        public void RunNormalTests(TestFilter filter = null)
        {
            //Clear current results
            TestResults.Clear();
            
            foreach (TestEntry testEntry in testEntries.Values)
            {
                TestResults.AddRange(testRunner.RunTestEntry(testEntry, filter));
            }
        }

        /// <summary>
        /// Runs all play mode loaded tests.
        /// Normal tests are ignored.
        /// </summary>
        public void RunPlayModeTests(TestFilter filter = null)
        {
            //Save that we are running play mode tests
            EditorPrefs.SetBool(PM_TESTING_KEY, true);
            
            //Run all play mode tests
            playModeTestRunner.RunEntriesTests(testEntries.Values, filter);
        }
        
        /// <summary>
        /// Runs all loaded tests.
        /// Runs both play mode and normal tests.
        /// The results are stored in the test results dictionary.
        /// </summary>
        public void RunAllTests(TestFilter filter = null)
        {
            //Clear current results
            TestResults.Clear();
            
            foreach (TestEntry testEntry in testEntries.Values)
            {
                TestResults.AddRange(testRunner.RunTestEntry(testEntry, filter));
            }
            
            //Save that we are running play mode tests
            EditorPrefs.SetBool(PM_TESTING_KEY, true);
            
            //Run all play mode tests
            playModeTestRunner.RunEntriesTests(testEntries.Values, filter);
        }

        /// <summary>
        /// Scans all assemblies to look for test methods.
        /// Works in a thread
        /// </summary>
        public void ThreadedBuildReferences(Action onCompleted, TestFilter filter = null)
        {
            Thread builder = new Thread(() =>
            {
                BuildTestReferencesList(filter);
                onCompleted?.Invoke();
            });
            builder.Start();
        }
        
        /// <summary>
        /// Scans all assemblies to look for test methods.
        /// </summary>
        public void BuildTestReferencesList(TestFilter filter = null)
        {
            testEntries.Clear();
            TestResults.Clear();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    //Check all fields for annotated values
                    foreach (MethodInfo methodInfo in type.GetMethods(
                        BindingFlags.Public |
                        BindingFlags.NonPublic |
                        BindingFlags.Instance))
                    {
                        //Skip methods that are not annotated
                        if(methodInfo.GetCustomAttributes(false).Length == 0) continue;
                        
                        //Skip methods that fail the filter
                        if(filter != null && !filter.Check(methodInfo)) continue;
                        
                        //Understand the method
                        bool isPlayModeTest = IsPlayModeTestMethod(methodInfo);
                        bool isTestMethod = IsTestMethod(methodInfo);
                        bool isTestSetupMethod = IsTestSetupMethod(methodInfo);
                        
                        //Check if the field is annotated
                        if (!isTestMethod && !isTestSetupMethod && !isPlayModeTest) continue;
                        
                        //Create the test entry
                        //This code is common to setup and test methods
                        TestEntry currentTestEntry;
                        if (testEntries.ContainsKey(type))
                        {
                            currentTestEntry = testEntries[type];
                        }
                        else
                        {
                            currentTestEntry = new TestEntry
                            {
                                TestType = type,
                                TestMethods = new List<MethodInfo>(),
                                PlayModeTestMethods = new List<MethodInfo>()
                            };

                            testEntries[type] = currentTestEntry;
                        }

                        //Setup setup methods
                        if (PrepareSetupMethod(methodInfo, currentTestEntry))
                        {
                            continue;
                        }

                        //If it is a play mode test add to the correct list
                        if (isPlayModeTest &&
                            !currentTestEntry.PlayModeTestMethods.Contains(methodInfo))
                        {
                            currentTestEntry.PlayModeTestMethods.Add(methodInfo);
                            TestResults[methodInfo] = TestResultInfo.None(true);
                            continue;
                        }

                        //If it is a test method, add it to the test methods list
                        if (!isTestMethod)
                        {
                            continue;
                        }
                        
                        //Only add the field once to that list
                        if (currentTestEntry.TestMethods.Contains(methodInfo))
                        {
                            continue;
                        }
                        currentTestEntry.TestMethods.Add(methodInfo);
                        TestResults[methodInfo] = TestResultInfo.None();
                    }
                }
            }
        }

        /// <summary>
        /// Reads a method to check if it is a setup method.
        /// Prepares that setup method to the test entry.
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="currentTestEntry"></param>
        /// <returns></returns>
        private static bool PrepareSetupMethod(MethodInfo methodInfo, TestEntry currentTestEntry)
        {
            if (IsBeforeClassSetupMethod(methodInfo))
            {
                if (currentTestEntry.BeforeClassMethod != null)
                {
                    Debug.LogWarning($"Method {methodInfo.Name} is" +
                                     $" overriding setup method" +
                                     $" {currentTestEntry.BeforeClassMethod.Name}!");
                }

                currentTestEntry.BeforeClassMethod = methodInfo;
                return true;
            }

            if (IsBeforeTestSetupMethod(methodInfo))
            {
                if (currentTestEntry.BeforeMethod != null)
                {
                    Debug.LogWarning($"Method {methodInfo.Name} is" +
                                     $" overriding setup method" +
                                     $" {currentTestEntry.BeforeMethod.Name}!");
                }

                currentTestEntry.BeforeMethod = methodInfo;
                return true;
            }

            if (IsAfterTestSetupMethod(methodInfo))
            {
                if (currentTestEntry.AfterMethod != null)
                {
                    Debug.LogWarning($"Method {methodInfo.Name} is" +
                                     $" overriding setup method" +
                                     $" {currentTestEntry.AfterMethod.Name}!");
                }

                currentTestEntry.AfterMethod = methodInfo;
                return true;
            }

            if (IsAfterClassSetupMethod(methodInfo))
            {
                if (currentTestEntry.AfterClassMethod != null)
                {
                    Debug.LogWarning($"Method {methodInfo.Name} is" +
                                     $" overriding setup method" +
                                     $" {currentTestEntry.AfterClassMethod.Name}!");
                }

                currentTestEntry.AfterClassMethod = methodInfo;
                return true;
            }

            return false;
        }

        private static bool IsPlayModeTestMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(PlayModeTest), false);
        }
        
        private static bool IsTestMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(Test), false);
        }
        
        private static bool IsTestSetupMethod(ICustomAttributeProvider methodInfo)
        {
            return 
                methodInfo.IsDefined(typeof(Before), false) ||
                methodInfo.IsDefined(typeof(BeforeClass), false) ||
                methodInfo.IsDefined(typeof(After), false) ||
                methodInfo.IsDefined(typeof(AfterClass), false);
        }
        
        private static bool IsBeforeTestSetupMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(Before), false);
        }
        
        private static bool IsBeforeClassSetupMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(BeforeClass), false);
        }
        
        private static bool IsAfterTestSetupMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(After), false);
        }
        
        private static bool IsAfterClassSetupMethod(ICustomAttributeProvider methodInfo)
        {
            return methodInfo.IsDefined(typeof(AfterClass), false);
        }
        
        //Tip: is faster to query the MethodInfo for the attribute than to use the Attribute Class
    }
}
