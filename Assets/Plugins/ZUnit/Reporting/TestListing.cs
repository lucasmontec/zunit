using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace ZUnit.Reporting
{
    [Serializable]
    public class TestListing
    {
        public string methodName;

        public TestResultInfo testResult;

        public TestListing(string methodName, TestResultInfo testResult)
        {
            this.methodName = methodName;
            this.testResult = testResult;
        }
        
    }
}