namespace ZUnit.Editor.Reporting
{
    public enum ReportType
    {
        Markdown,
        Json
    }
}