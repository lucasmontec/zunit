using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Tiny;
using UnityEngine;
using ZUnit.Reporting;

namespace ZUnit.Editor.Reporting
{
    public static class TestReporter
    {

        [Serializable]
        private class JsonTestReport
        {
            public DateTime date;
            public int failedTests;
            public int passedTests;
            
            public Dictionary<Type, List<TestListing>> tests;
        }
        
        public static void SaveJsonReport(TestSuite testSuite)
        {
            testSuite.LoadTestResults();

            DateTime today = DateTime.Now;
            
            JsonTestReport report = new JsonTestReport
            {
                passedTests = testSuite.TestResults.Count(res => res.Value.result == TestResult.Passed),
                failedTests = testSuite.TestResults.Count
                    (res => res.Value.result == TestResult.Failed ||
                            res.Value.result == TestResult.Inconclusive ||
                            res.Value.result == TestResult.FailedToRun),
                date = today,
                tests = TypeToTestResult(testSuite)
            };

            string json = report.Encode(true);
            File.WriteAllText($"TestReport_{today:dd_MM_yyyy}.json", json);
            Debug.Log($"Report generated at " +
                      $"{Application.dataPath.Replace("Assets/",string.Empty)}" +
                      $"/TestReport_{today:dd_MM_yyyy}.json");
        }
        
        public static void SaveMarkdownReport(TestSuite testSuite)
        {
            testSuite.LoadTestResults();
            
            StringBuilder testResultFile = new StringBuilder();
            
            //Make header

            DateTime today = DateTime.Now;

            int passedTests = testSuite.TestResults.Count(res => res.Value.result == TestResult.Passed);
            int failedTests = testSuite.TestResults.Count
            (res => res.Value.result == TestResult.Failed ||
                    res.Value.result == TestResult.Inconclusive ||
                    res.Value.result == TestResult.FailedToRun);
            
            testResultFile.AppendLine("----------------------------------");
            testResultFile.AppendLine("# Test Report");
            testResultFile.AppendLine($"## Test Date {today:dd/MM/yyyy HH:mm}");
            testResultFile.AppendLine($"## Passed tests {passedTests}");
            testResultFile.AppendLine($"## Failed tests {failedTests}");
            testResultFile.AppendLine("----------------------------------");
            testResultFile.AppendLine();
            
            var typeToTestResult = TypeToTestResult(testSuite);

            //Build the report
            foreach (KeyValuePair<Type,List<TestListing>> typeToTests in typeToTestResult)
            {
                testResultFile.AppendLine($"### {typeToTests.Key}");
                testResultFile.AppendLine();
                foreach (TestListing testListing in typeToTests.Value)
                {
                    testResultFile.AppendLine($"Test method {testListing.methodName}" +
                                              $"{(testListing.testResult.playModeTest?"(Play Mode Test)":"")}:" +
                                              $" {testListing.testResult.result}");
                    
                    if (testListing.testResult.result != TestResult.Failed) continue;
                    testResultFile.AppendLine($" > Failed at line {testListing.testResult.failLineNumber}" +
                                              $" in file {testListing.testResult.filePath}");
                    testResultFile.AppendLine();
                }
                testResultFile.AppendLine();
                testResultFile.AppendLine("----------------------------------");
            }
            
            File.WriteAllText($"TestReport_{today:dd_MM_yyyy}.md", testResultFile.ToString());
            Debug.Log($"Report generated at " +
                      $"{Directory.GetCurrentDirectory().Replace("\\", "/")}" +
                      $"/TestReport_{today:dd_MM_yyyy}.md");
        }

        private static Dictionary<Type, List<TestListing>> TypeToTestResult(TestSuite testSuite)
        {
            //Create a test map to get tests with the same class bundles
            Dictionary<Type, List<TestListing>> typeToTestResult = new Dictionary<Type, List<TestListing>>();

            //Create the listing list to build the report
            foreach (KeyValuePair<MethodInfo, TestResultInfo> testPair in testSuite.TestResults)
            {
                MethodInfo method = testPair.Key;
                TestResultInfo result = testPair.Value;

                Type testType = method.DeclaringType;
                if (testType == null) continue;

                if (!typeToTestResult.ContainsKey(testType))
                {
                    typeToTestResult[testType] = new List<TestListing>();
                }

                typeToTestResult[testType].Add(new TestListing(method.Name, result));
            }

            return typeToTestResult;
        }
    }
}