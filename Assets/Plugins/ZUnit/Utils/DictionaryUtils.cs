using System;
using System.Collections.Generic;

namespace ZUnit.Utils
{
    public static class DictionaryUtils
    {
        public static void AddRange<TK,TV>(this IDictionary<TK,TV> target, IDictionary<TK,TV> source)
        {
            if(target==null)
                throw new ArgumentNullException(nameof(target));
            if(source==null)
                throw new ArgumentNullException(nameof(source));

            foreach (var element in source)
            {
                target[element.Key] = element.Value;
            }
        }
    }
}