﻿namespace ZUnit
{
    /// <summary>
    /// The possible test results.
    /// </summary>
    public enum TestResult
    {
        None,
        Passed,
        Failed,
        Inconclusive,
        FailedToRun
    }
}
