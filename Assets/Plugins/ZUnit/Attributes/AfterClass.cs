using System;

namespace ZUnit.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class AfterClass : Attribute { }
}