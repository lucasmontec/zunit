using System;

namespace ZUnit.Attributes
{
    /// <summary>
    /// To be added to a method to run the method before every test.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class Before: Attribute { }
}