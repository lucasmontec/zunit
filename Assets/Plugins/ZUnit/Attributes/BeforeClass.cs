using System;

namespace ZUnit.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class BeforeClass : Attribute { }
}