using System;

namespace ZUnit.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class PlayModeTest : Attribute { }
}