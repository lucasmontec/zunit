namespace ZUnit.Filtering
{
    public enum TestFilterType
    {
        None,
        Namespace,
        ClassName,
        MethodName
    }
}