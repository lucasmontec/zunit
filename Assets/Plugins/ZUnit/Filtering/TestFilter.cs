using System;
using System.Reflection;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace ZUnit.Filtering
{
    [HideLabel]
    [BoxGroup("Filter")]
    [Serializable]
    public class TestFilter
    {
        [HideLabel, HorizontalGroup("Filter")]
        [OnValueChanged(nameof(FilterChanged))]
        public TestFilterType filterType;
        
        [HorizontalGroup("settings")]
        [Tooltip("Match is case sensitive if this is set to true.")]
        [OnValueChanged(nameof(FilterChanged))]
        public bool caseSensitive;

        [HorizontalGroup("settings")]
        [Tooltip("Matches if the name contains the filter instead of exact.")]
        [OnValueChanged(nameof(FilterChanged))]
        public bool contains;
        
        [HorizontalGroup("Filter", MinWidth = 0.62f)]
        [HideLabel]
        [DisableIf(nameof(filterType), TestFilterType.None)]
        [OnValueChanged(nameof(FilterChanged)), Delayed]
        public string filterValue;

        public event Action OnFilterChanged;

        private void FilterChanged()
        {
            OnFilterChanged?.Invoke();
        }
        
        [Button]
        [HorizontalGroup("Filter", MinWidth = 0.12f)]
        public void Clear()
        {
            filterValue = "";
            filterType = TestFilterType.None;
        }
        
        public TestFilter(TestFilterType filterType, string filterValue)
        {
            this.filterType = filterType;
            this.filterValue = filterValue;
        }

        public bool Check(MethodInfo methodInfo)
        {
            switch (filterType)
            {
                case TestFilterType.Namespace:
                    return CompareFilterValue(methodInfo.DeclaringType?.Namespace);
                case TestFilterType.ClassName:
                    return CompareFilterValue(methodInfo.DeclaringType?.Name);
                case TestFilterType.MethodName:
                    return CompareFilterValue(methodInfo.Name);
                case TestFilterType.None:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool CompareFilterValue(string expected)
        {
            if (expected == null) return false;

            bool wildcard = filterValue.Contains("*");
            var compareValue = filterValue.Replace("*", string.Empty);

            //If match case
            if (caseSensitive) return wildcard || contains ? expected.Contains(compareValue) : expected == compareValue;
            
            //Else, ignore case
            expected = expected.ToLower();
            compareValue = compareValue.ToLower();

            return wildcard || contains ? expected.Contains(compareValue) : expected == compareValue;
        }
    }
}