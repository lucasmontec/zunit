using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;
using ZUnit.Attributes;
using ZUnit.Editor.Reporting;
using ZUnit.Editor.TestRunning;
using ZUnit.TestRunning;

namespace ZUnit.Editor
{
    /// <summary>
    /// A test runner that is made to be called by a headless unity instance.
    /// </summary>
    [InitializeOnLoad]
    public static class IntegrationRunner
    {
        private static readonly TestSuite TestSuite = new TestSuite(new TestRunner(), new PlayModeTestRunner());

        static IntegrationRunner()
        {
            EditorApplication.playModeStateChanged += change =>
            {
                if (change != PlayModeStateChange.EnteredEditMode) return;

                Debug.Log("Back from play mode.");
                
                TestSuite.LoadTestResults(out var reportAndClose, out var reportType);

                Debug.Log($"Report and close: {reportAndClose}.");
                
                if (!reportAndClose) return;
                    
                switch (reportType)
                {
                    case ReportType.Markdown:
                        Debug.Log("Saving markdown report.");
                        TestReporter.SaveMarkdownReport(TestSuite);
                        break;
                    case ReportType.Json:
                        Debug.Log("Saving json report.");
                        TestReporter.SaveJsonReport(TestSuite);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                int failedTests = TestSuite.TestResults.Any(res => res.Value.result == TestResult.Failed ||
                                                                       res.Value.result == TestResult.Inconclusive ||
                                                                       res.Value.result == TestResult.FailedToRun)
                    ? 1  //Error
                    : 0; //Success
                
                Debug.Log($"Closing editor with exit status {failedTests}.");
                
                EditorApplication.Exit(failedTests);
            };
        }
        
        private static void BuildTestReferencesList()
        {
            TestSuite.BuildTestReferencesList();
        }
        
        public static void RunPlayModeTests()
        {
            BuildTestReferencesList();
            TestResultsSerializer.RemoveSavedResults();
            TestSuite.RunPlayModeTestsReportAndClose(ReportType.Markdown);
            
            TestReporter.SaveMarkdownReport(TestSuite);
        }
        
        public static void RunPlayModeTestsJson()
        {
            BuildTestReferencesList();
            TestResultsSerializer.RemoveSavedResults();
            TestSuite.RunPlayModeTestsReportAndClose(ReportType.Json);
            
            TestReporter.SaveMarkdownReport(TestSuite);
        }
        
        public static void RunNormalTests()
        {
            BuildTestReferencesList();
            TestResultsSerializer.RemoveSavedResults();
            TestSuite.RunNormalTests();
            
            TestReporter.SaveMarkdownReport(TestSuite);
        }
        
        public static void RunAllTests()
        {
            BuildTestReferencesList();
            TestResultsSerializer.RemoveSavedResults();
            TestSuite.RunAllTests();
            
            TestReporter.SaveMarkdownReport(TestSuite);
        }
    }
}