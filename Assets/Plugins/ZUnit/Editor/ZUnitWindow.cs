using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;
using ZUnit.Editor.Reporting;
using ZUnit.Editor.TestRunning;
using ZUnit.Filtering;
using ZUnit.TestRunning;
using ZUnit.Utils;

namespace ZUnit.Editor
{
    [InitializeOnLoad]
    public class ZUnitWindow : OdinEditorWindow, IHasCustomMenu
    {
        private readonly TestSuite testSuite = new TestSuite(new TestRunner(), new PlayModeTestRunner());

        [PropertyOrder(0)]
        public TestFilter testFilter;
        
        [TableList(IsReadOnly = true, AlwaysExpanded = true, ShowPaging = true, NumberOfItemsPerPage = 10)]
        [SerializeField]
        [PropertyOrder(2)]
        private List<EditorTestListing> tests = new List<EditorTestListing>();

        private bool buildingReferences;

        [HideInInspector]
        public bool scanOnFilterChanges = true;
        
        static ZUnitWindow()
        {
            EditorApplication.playModeStateChanged += change =>
            {
                if(!TestSuite.DidPlayModeTests) return;
                
                ZUnitWindow testWindow = GetWindow<ZUnitWindow>();
                
                if(testWindow == null) return;
                
                //Load saved results
                testWindow.LoadTestResults();
                
                TestSuite.ResetPlayModeTestsFlag();
            };
        }
        
        [MenuItem("Window/ZUnit Test Window")]
        private static void OpenWindow()
        {
            GetWindow<ZUnitWindow>().Show();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            AssemblyReloadEvents.afterAssemblyReload += AfterAssemblyReload;

            LoadTestResults();

            testFilter.OnFilterChanged += OnFilterChanged;
        }

        private void OnFilterChanged()
        {
            if (scanOnFilterChanges)
            {
                BuildTestReferencesList();
            }
        }
        
        private void LoadTestResults()
        {
            //Load saved results
            EditorApplication.delayCall += () =>
            {
                testSuite.LoadTestResults();
                LoadTestListings();
            };
        }

        private void OnDisable()
        {
            AssemblyReloadEvents.afterAssemblyReload -= AfterAssemblyReload;
            
            testFilter.OnFilterChanged -= OnFilterChanged;
        }

        private void AfterAssemblyReload()
        {
            buildingReferences = false;
        }

        [Button("Scan Tests",ButtonSizes.Large)]
        [PropertyOrder(1)]
        [DisableIf(nameof(buildingReferences))]
        [ButtonGroup]
        private void BuildTestReferencesList()
        {
            if(buildingReferences) return;
            
            buildingReferences = true;
            testSuite.ThreadedBuildReferences(() =>
            {
                buildingReferences = false;
                LoadTestListings();
            }, testFilter);
        }
        
        [PropertyOrder(1)]
        [Button("Reset",ButtonSizes.Large)]
        [ButtonGroup]
        private void ResetTests()
        {
            foreach (EditorTestListing listing in tests)
            {
                listing.testResult = TestResultInfo.None(listing.testResult.playModeTest);
            }
            
            TestResultsSerializer.RemoveSavedResults();
        }
        
        private void BuildTestReferencesListImmediate()
        {
            if(buildingReferences) return;
            buildingReferences = true;
            testSuite.BuildTestReferencesList(testFilter);
            buildingReferences = false;
        }

        [PropertyOrder(3)]
        [Button("Run Play Mode",ButtonSizes.Large)]
        [ButtonGroup("run")]
        [GUIColor(0.5f, 0.5f, 1)]
        private void RunPlayModeTests()
        {
            BuildTestReferencesListImmediate();
            TestResultsSerializer.RemoveSavedResults();
            testSuite.RunPlayModeTests(testFilter);
            LoadTestListings();
        }
        
        [PropertyOrder(3)]
        [Button("Run Normal",ButtonSizes.Large)]
        [ButtonGroup("run")]
        [GUIColor(0, 1, 0)]
        private void RunNormalTests()
        {
            BuildTestReferencesListImmediate();
            TestResultsSerializer.RemoveSavedResults();
            testSuite.RunNormalTests(testFilter);
            LoadTestListings();
        }
        
        [PropertyOrder(3)]
        [Button("Run All",ButtonSizes.Large)]
        [ButtonGroup("run")]
        [GUIColor(1, 0.5f, 0.5f)]
        private void RunAllTests()
        {
            BuildTestReferencesListImmediate();
            TestResultsSerializer.RemoveSavedResults();
            testSuite.RunAllTests(testFilter);
            LoadTestListings();
        }

        private bool CheckIfCanReport()
        {
            return testSuite != null && testSuite.TestResults.Count(t => t.Value.result != TestResult.None) > 0;
        }
        
        [PropertyOrder(4)]
        [EnableIf(nameof(CheckIfCanReport))]
        [Button(ButtonSizes.Large)]
        [ButtonGroup("report")]
        public void SaveMarkdownReport()
        {
            TestReporter.SaveMarkdownReport(testSuite);
        }
        
        [PropertyOrder(4)]
        [EnableIf(nameof(CheckIfCanReport))]
        [Button(ButtonSizes.Large)]
        [ButtonGroup("report")]
        public void SaveJsonReport()
        {
            TestReporter.SaveJsonReport(testSuite);
        }
        
        public void RunTests(TestFilter filter)
        {
            BuildTestReferencesListImmediate();
            TestResultsSerializer.RemoveSavedResults();
            testSuite.RunAllTests(filter);
            LoadTestListings();
        }
        
        private void LoadTestListings()
        {
            tests.Clear();
            foreach (KeyValuePair<MethodInfo, TestResultInfo> testPair in testSuite.TestResults)
            {
                MethodInfo method = testPair.Key;
                TestResultInfo result = testPair.Value;
                
                tests.Add(new EditorTestListing(method.GetNiceName(), result));
            }
        }

        public void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Rescan on filter changes"), scanOnFilterChanges, 
                () =>
                {
                    scanOnFilterChanges = !scanOnFilterChanges;
                });
        }
    }
}