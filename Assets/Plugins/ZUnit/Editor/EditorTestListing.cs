using System;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using ZUnit.Filtering;

namespace ZUnit.Editor
{
    [Serializable]
    public class EditorTestListing
    {
        [CustomValueDrawer(nameof(TestNameDrawer))]
        public string methodName;
        
        [CustomValueDrawer(nameof(TestResultDrawer))]
        public TestResultInfo testResult;

        private static GUIStyle defaultTestStyle;
        
        private static GUIStyle testPassedStyle;
        
        private static GUIStyle testFailedStyle;

        private static Color passedColor = new Color(0.1f, 0.4f, 0.1f);
        
        private static Color failedColor = new Color(0.5f, 0.1f, 0.1f);
        
        private static void PrepareStyles()
        {
            passedColor = new Color(0.1f, 0.4f, 0.1f);
            failedColor = new Color(0.5f, 0.1f, 0.1f);
            
            defaultTestStyle = new GUIStyle
            {
                alignment = TextAnchor.MiddleCenter, 
                fontSize = 17, 
                fontStyle = FontStyle.Bold,
                normal =
                {
                    textColor = Color.white
                }
            };

            testPassedStyle = new GUIStyle()
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 12, 
                fontStyle = FontStyle.Bold,
                normal =
                {
                    textColor = Color.white,
                    background = MakeTex(1,1, passedColor)
                }
            };
            
            testFailedStyle = new GUIStyle()
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 12, 
                fontStyle = FontStyle.Bold,
                margin =
                {
                  top  = 5,
                  bottom = 5,
                  left = 4,
                  right = 0
                },
                normal =
                {
                    textColor = Color.white,
                    background = MakeTex(1,1, failedColor)
                }
            };
        }
        
        private static string TestNameDrawer(string testName, GUIContent label)
        {
            EditorGUILayout.LabelField(testName);
            return testName;
        }
        
        private static TestResultInfo TestResultDrawer(TestResultInfo resultInfo, GUIContent label)
        {
            TestResult result = resultInfo.result;
            
            string resultString = result.ToString();

            PrepareStyles();
            
            GUIStyle testStyle = defaultTestStyle;

            switch (result)
            {
                case TestResult.None:
                    resultString = "-";
                    break;
                case TestResult.Passed:
                    testStyle = testPassedStyle;
                    break;
                case TestResult.Failed:
                    testStyle = testFailedStyle;
                    break;
                case TestResult.Inconclusive:
                    break;
                case TestResult.FailedToRun:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(result), result, null);
            }
            
            EditorGUILayout.BeginHorizontal();

            //Add the code button if the test fails
            if (result == TestResult.Failed)
            {
                if (GUILayout.Button(resultString, testStyle, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
                {
                    Debug.LogError($"Test running exception: {resultInfo.exception.Message}" +
                                   $"\n{resultInfo.exception.StackTrace}");
                }
                if (GUILayout.Button("Code", GUILayout.MinHeight(20), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true)))
                {
                    var file = AssetDatabase.LoadMainAssetAtPath(resultInfo.filePath);
                    AssetDatabase.OpenAsset(file, resultInfo.failLineNumber);
                }

                if (!string.IsNullOrEmpty(resultInfo.methodName))
                {
                    if (GUILayout.Button("Run", GUILayout.MinHeight(20), GUILayout.ExpandWidth(false),
                        GUILayout.ExpandHeight(true)))
                    {
                        ZUnitWindow testWindow = EditorWindow.GetWindow<ZUnitWindow>();
                        if (testWindow)
                        {
                            TestFilter filter = new TestFilter(TestFilterType.MethodName, resultInfo.methodName);
                            testWindow.RunTests(filter);
                        }
                    }
                }
            }
            else
            {
                EditorGUILayout.LabelField(resultString, testStyle, GUILayout.ExpandWidth(true));
            }

            EditorGUILayout.EndHorizontal();
            
            return resultInfo;
        }
        
        public EditorTestListing(string methodName, TestResultInfo testResult)
        {
            this.methodName = methodName;
            this.testResult = testResult;
        }
        
        private static Texture2D MakeTex(int width, int height, Color col) {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();

            return result;
        }
    }
}