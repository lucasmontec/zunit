using System;
using System.Collections.Generic;
using System.Reflection;

namespace ZUnit
{
    public class TestEntry
    {
        public Type TestType;
        public List<MethodInfo> TestMethods;
        public List<MethodInfo> PlayModeTestMethods;
            
        public MethodInfo BeforeMethod;
        public MethodInfo AfterMethod;
            
        public MethodInfo BeforeClassMethod;
        public MethodInfo AfterClassMethod;
    }
}