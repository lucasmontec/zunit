﻿using System;

namespace ZUnit
{
    public static class ZAssert
    {
        public static void IsNotInstanceOfType(object actual, Type t, string messageOnFail = "Object is an instance of type.")
        {
            IsNotNull(actual, "Object cannot be null when checking inheritance.");
        
            if (actual.GetType().IsInstanceOfType(t))
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }
    
        public static void IsInstanceOfType(object actual, Type t, string messageOnFail = "Object is not an instance of type.")
        {
            IsNotNull(actual, "Object cannot be null when checking inheritance.");
        
            if (!actual.GetType().IsInstanceOfType(t))
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }
    
        public static void IsNotNull(object actual, string messageOnFail = "Object is null.")
        {
            if (actual == null)
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }
    
        public static void IsNull(object actual, string messageOnFail = "Object is not null.")
        {
            if (actual != null)
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }
    
        public static void AreNotEqual<T>(T actual, T expected)
        {
            if (actual == null && expected == null)
            {
                throw new AssertionFailedException("Both actual and expected are null (equal).");
            }
        
            if (ReferenceEquals(actual, expected))
            {
                throw new AssertionFailedException("Actual and expected are the same reference.");
            }
        
            if (actual != null && actual.Equals(expected))
            {
                throw new AssertionFailedException($"Actual ({actual}) is equal to expected ({expected}).");
            }
        }
    
        public static void AreNotEqual(object actual, object expected)
        {
            if (actual == null && expected == null)
            {
                throw new AssertionFailedException("Both actual and expected are null (equal).");
            }
        
            if (ReferenceEquals(actual, expected))
            {
                throw new AssertionFailedException("Actual and expected are the same reference.");
            }
        
            if (actual == expected)
            {
                throw new AssertionFailedException($"Actual ({actual}) is equal to expected ({expected}).");
            }
        }
    
        public static void AreEqual<T>(T actual, T expected)
        {
            if (actual == null && expected != null)
            {
                throw new AssertionFailedException("Actual is null while expected isn't.");
            }
        
            if (actual != null && !actual.Equals(expected))
            {
                throw new AssertionFailedException($"Actual ({actual}) is different from expected ({expected}).");
            }
        }
    
        public static void AreEqual(object actual, object expected)
        {
            if (actual == null && expected != null)
            {
                throw new AssertionFailedException("Actual is null while expected isn't.");
            }
        
            if (actual != expected)
            {
                throw new AssertionFailedException($"Actual ({actual}) is different from expected ({expected}).");
            }
        }

        public static void IsFalse(bool value, string messageOnFail = "Expression is not false.")
        {
            if (value)
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }
    
        public static void IsTrue(bool value, string messageOnFail = "Expression is not true.")
        {
            if (!value)
            {
                throw new AssertionFailedException(messageOnFail);
            }
        }

        public static void Inconclusive(string message = "Inconclusive")
        {
            throw new AssertionInconclusiveException(message);
        }

        public static void Fail(string message = "Failed")
        {
            throw new AssertionFailedException(message);
        }

        public class AssertionException : Exception
        {
            public AssertionException()
            {
            }

            public AssertionException(string message) : base(message)
            {
            }

            public AssertionException(string message, Exception innerException) : base(message, innerException)
            {
            }
        }
    
        public class AssertionInconclusiveException : AssertionException
        {
            public AssertionInconclusiveException(string message) : base(message)
            {
            }
        
            public AssertionInconclusiveException()
            {
            }
        }
    
        public class AssertionFailedException : AssertionException
        {
            public AssertionFailedException(string message) : base(message)
            {
            }
        
            public AssertionFailedException()
            {
            }
        }
    
    }
}
