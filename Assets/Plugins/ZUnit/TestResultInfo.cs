using System;
using UnityEngine.Serialization;

namespace ZUnit
{
    /// <summary>
    /// Stores the test result information.
    /// Stores the test result along with the
    /// file, error line and path to a failing test.
    /// </summary>
    [Serializable]
    public struct TestResultInfo
    {
        public TestResult result;
        
        public int failLineNumber;
        
        public string filePath;
        public string typeName;
        public string methodName;
        
        public bool playModeTest;

        public Exception exception;

        public static TestResultInfo None(bool playMode=false)
        {
            return new TestResultInfo()
            {
                result = TestResult.None,
                playModeTest = playMode
            };
        }
        
        public static TestResultInfo Inconclusive(bool playMode=false)
        {
            return new TestResultInfo()
            {
                result = TestResult.Inconclusive,
                playModeTest = playMode
            };
        }
    }
}