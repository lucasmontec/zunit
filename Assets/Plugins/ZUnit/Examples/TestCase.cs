﻿using UnityEngine;
using ZUnit;
using ZUnit.Attributes;
using static ZUnit.ZAssert;

public class TestCase
{
    private string a, b;
    
    [BeforeClass]
    public void Prepare()
    {
        Debug.Log("> Before Class");
        a = "test";
        b = "test";
    }
    
    [AfterClass]
    public void TearDown()
    {
        Debug.Log("> After Class");
    }
    
    [Before]
    public void PrepareMethod()
    {
        Debug.Log(">> Before Method");
    }
    
    [After]
    public void TearDownMethod()
    {
        Debug.Log(">> After Method");
    }
    
    [Test]
    public void Test_SomeTest()
    {
        Debug.Log("LOL");
    }
    
    [PlayModeTest]
    public void Test_PlayModeTest()
    {
        Debug.Log("PLAY MODE TEST");
    }
    
    [Test]
    public void Test_Fail()
    {
        Fail();
    }
    
    [Test]
    public void Test_Prepared()
    {
        AreEqual(a, b);
    }
}
