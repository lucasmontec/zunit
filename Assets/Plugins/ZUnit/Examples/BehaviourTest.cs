﻿using System.Collections;
using UnityEngine;
using ZUnit.Attributes;
using static ZUnit.ZAssert;

namespace ZUnit.Examples
{
    public class BehaviourTest : MonoBehaviour
    {
        [PlayModeTest]
        public void Test_PlayModeTest2()
        {
            Debug.Log(transform.position);
        }
        
        [PlayModeTest]
        public IEnumerator Test_CoroutineTest()
        {
            Debug.Log("Started coroutine test");
            yield return new WaitForSeconds(1f);
            Debug.Log("Ended coroutine test");
        }

        [PlayModeTest]
        public IEnumerator Test_CoroutineFailTest()
        {
            yield return new WaitForSeconds(2f);
            Fail();
        }
        
    }
}
